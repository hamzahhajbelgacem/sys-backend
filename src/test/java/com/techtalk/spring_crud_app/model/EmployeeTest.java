package com.techtalk.spring_crud_app.model;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class EmployeeTest {

    @Test
    public void testNoArgsConstructor() {
        Employee employee = new Employee();

        assertThat(employee.getId()).isEqualTo(0);
        assertThat(employee.getName()).isNull();
        assertThat(employee.getEmail()).isNull();
        assertThat(employee.getSalary()).isEqualTo(0);
    }

    @Test
    public void testAllArgsConstructor() {
        Employee employee = new Employee(1, "John", "john@example.com", 50000);

        assertThat(employee.getId()).isEqualTo(1);
        assertThat(employee.getName()).isEqualTo("John");
        assertThat(employee.getEmail()).isEqualTo("john@example.com");
        assertThat(employee.getSalary()).isEqualTo(50000);
    }

    @Test
    public void testGettersAndSetters() {
        Employee employee = new Employee();

        employee.setId(2);
        employee.setName("Jane");
        employee.setEmail("jane@example.com");
        employee.setSalary(60000);

        assertThat(employee.getId()).isEqualTo(2);
        assertThat(employee.getName()).isEqualTo("Jane");
        assertThat(employee.getEmail()).isEqualTo("jane@example.com");
        assertThat(employee.getSalary()).isEqualTo(60000);
    }
}
