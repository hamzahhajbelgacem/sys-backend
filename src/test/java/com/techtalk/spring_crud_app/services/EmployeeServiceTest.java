package com.techtalk.spring_crud_app.services;

import com.techtalk.spring_crud_app.model.Employee;
import com.techtalk.spring_crud_app.repository.EmployeeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyIterable;
import static org.mockito.Mockito.*;

public class EmployeeServiceTest {

    @Mock
    private EmployeeRepository employeeRepository;

    @InjectMocks
    private EmployeeService employeeService;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testAddEmployee() {
        Employee employee = new Employee(1, "John", "john@example.com", 50000);
        when(employeeRepository.save(any(Employee.class))).thenReturn(employee);

        Employee savedEmployee = employeeService.addEmployee(employee);
        assertThat(savedEmployee).isEqualTo(employee);
    }

    @Test
    public void testAddAllEmployees() {
        List<Employee> employees = new ArrayList<>();
        employees.add(new Employee(1, "John", "john@example.com", 50000));
        employees.add(new Employee(2, "Jane", "jane@example.com", 60000));
        when(employeeRepository.saveAll(anyIterable())).thenReturn(employees);

        List<Employee> savedEmployees = employeeService.addAllEmployees(employees);
        assertThat(savedEmployees).isEqualTo(employees);
    }

    @Test
    public void testGetEmployeeByID() {
        Employee employee = new Employee(1, "John", "john@example.com", 50000);
        when(employeeRepository.findById(1)).thenReturn(Optional.of(employee));

        Employee retrievedEmployee = employeeService.getEmployeeByID(1);
        assertThat(retrievedEmployee).isEqualTo(employee);
    }

    // Similar tests for other methods...

    @Test
    public void testUpdateEmployee() {
        Employee existingEmployee = new Employee(3, "Alex", "alex@example.com", 70000);
        when(employeeRepository.findById(3)).thenReturn(Optional.of(existingEmployee));

        Employee updatedEmployee = new Employee(3, "Updated Alex", "updated@example.com", 75000);
        when(employeeRepository.save(any(Employee.class))).thenReturn(updatedEmployee);

        Employee result = employeeService.updateEmployee(updatedEmployee);
        assertThat(result.getName()).isEqualTo("Updated Alex");
        assertThat(result.getEmail()).isEqualTo("updated@example.com");
        assertThat(result.getSalary()).isEqualTo(75000);
    }
}
