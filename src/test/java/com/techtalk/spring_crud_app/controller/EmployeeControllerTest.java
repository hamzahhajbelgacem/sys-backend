package com.techtalk.spring_crud_app.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.techtalk.spring_crud_app.model.Employee;
import com.techtalk.spring_crud_app.services.EmployeeService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(EmployeeController.class)
public class EmployeeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EmployeeService employeeService;

    @InjectMocks
    private EmployeeController employeeController;

    @BeforeEach
    public void setup() {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(employeeController).build();
    }

    @Test
    public void testAddEmployee() throws Exception {
        Employee employee = new Employee();
        employee.setId(1);
        employee.setName("John");
        employee.setEmail("salim@gmail.com");
        employee.setSalary(50000);

        when(employeeService.addEmployee(any(Employee.class))).thenReturn(employee);

        mockMvc.perform(post("/emp/addEmployee")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(employee)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("John"));
    }



    // Similar tests for other methods...

    @Test
    public void testGetAllEmployee() throws Exception {
        List<Employee> employeeList = new ArrayList<>();
        Employee employee1 = new Employee(1, "John","salim@gmail.com", 50000);
        Employee employee2 = new Employee(2, "Jane", "mahdi@gmail.com" , 60000);
        employeeList.add(employee1);
        employeeList.add(employee2);

        when(employeeService.getAllEmployees()).thenReturn(employeeList);

        mockMvc.perform(get("/emp/getAll"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name").value("John"))
                .andExpect(jsonPath("$[1].name").value("Jane"));
    }
}
