FROM adoptopenjdk:11-jre-hotspot
RUN mkdir -p /app
WORKDIR /app

COPY ./target/spring_crud_app-0.0.2-SNAPSHOT.jar  /app/spring_crud_app-0.0.2-SNAPSHOT.jar
CMD [ "java","-jar","/app/spring_crud_app-0.0.2-SNAPSHOT.jar "]