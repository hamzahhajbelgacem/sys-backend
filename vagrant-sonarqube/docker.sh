sudo yum update
# Install the EPEL repository
sudo yum install epel-release

# Install Docker
sudo yum install docker
# Start Docker
sudo systemctl start docker

# Enable Docker to start on boot
sudo systemctl enable docker

sudo docker --24.0.5



sudo usermod -aG docker vagrant
